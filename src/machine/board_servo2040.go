//go:build servo2040
// +build servo2040

// This contains the pin mappings for the Servo 2040 Connect board.
//
// For more information, see: https://shop.pimoroni.com/products/servo-2040
// Also
// - Servo 2040 schematic: https://cdn.shopify.com/s/files/1/0174/1800/files/servo2040_schematic.pdf
package machine

import (
	"device/rp"
	"runtime/interrupt"
)

// GENERAL PINS
const (
	LED_DATA_PIN Pin = GPIO18
	USER_SW          = GPIO23

	EPD_BUSY_PIN  Pin = GPIO26
	EPD_RESET_PIN Pin = GPIO21
	EPD_DC_PIN    Pin = GPIO20
	EPD_CS_PIN    Pin = GPIO17
	EPD_SCK_PIN   Pin = GPIO18
	EPD_SDO_PIN   Pin = GPIO19

	VBUS_DETECT Pin = GPIO24
	BATTERY     Pin = GPIO29
	ENABLE_3V3  Pin = GPIO10
)

// SERVO PINS
const (
	SERVO1  Pin = GPIO0
	SERVO2  Pin = GPIO1
	SERVO3  Pin = GPIO2
	SERVO4  Pin = GPIO3
	SERVO5  Pin = GPIO4
	SERVO6  Pin = GPIO5
	SERVO7  Pin = GPIO6
	SERVO8  Pin = GPIO7
	SERVO9  Pin = GPIO8
	SERVO10 Pin = GPIO9
	SERVO11 Pin = GPIO10
	SERVO12 Pin = GPIO11
	SERVO13 Pin = GPIO12
	SERVO14 Pin = GPIO13
	SERVO15 Pin = GPIO14
	SERVO16 Pin = GPIO15
	SERVO17 Pin = GPIO16
	SERVO18 Pin = GPIO17
)

// I2C pins
const (
	I2C0_INT_PIN Pin = GPIO19
	I2C0_SDA_PIN Pin = GPIO20
	I2C0_SCL_PIN Pin = GPIO21
)

// Analog to Digital Converter Pins
const (
	ADC_ADDR_0 = GPIO22
	ADC_ADDR_1 = GPIO24
	ADC_ADDR_2 = GPIO25
	ADC0       = GPIO26
	ADC1       = GPIO27
	ADC2       = GPIO28
	SHARED_ADC = GPIO29
)

// SPI pins
const (
	SPI0_SCK_PIN Pin = NoPin
	SPI0_SDO_PIN Pin = GPIO19
	SPI0_SDI_PIN Pin = GPIO16
)

// QSPI pins
const (
	SPI0_SD0_PIN Pin = QSPI_SD0
	SPI0_SD1_PIN Pin = QSPI_SD1
	SPI0_SD2_PIN Pin = QSPI_SD2
	SPI0_SD3_PIN Pin = QSPI_SD3
	SPI0_SCK_PIN Pin = QSPI_SCLKGPIO6
	SPI0_CS_PIN  Pin = QSPI_CS
)

// Onboard crystal oscillator frequency, in MHz.
const (
	xoscFreq = 12 // MHz
)

// USB CDC identifiers
const (
	usb_STRING_PRODUCT      = "Servo 2040"
	usb_STRING_MANUFACTURER = "Pimoroni"
)

var (
	usb_VID uint16 = 0x2e8a
	usb_PID uint16 = 0x0003
)

// UART pins
// Servo 2040 has no UART pins!
const (
	UART0_TX_PIN = NoPin
	UART0_RX_PIN = NoPin
	UART_TX_PIN  = NoPin
	UART_RX_PIN  = NoPin
)

// UART on the RP2040
var (
	UART0  = &_UART0
	_UART0 = UART{
		Buffer: NewRingBuffer(),
		Bus:    rp.UART0,
	}
)

var DefaultUART = UART0

func init() {
	UART0.Interrupt = interrupt.New(rp.IRQ_UART0_IRQ, _UART0.handleInterrupt)
}
